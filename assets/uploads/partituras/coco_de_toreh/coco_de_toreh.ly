\include "../formatoversos.ly"
\include "../marcaspadronizadas.ly"
\include "../nomedasnotas.ly"

%#(set-global-staff-size 17)

\header{
  title = "Coco de Toré"
  composer = \markup {
    \right-column {
      "Walê Fulni-ô"
      " "
    }
  }
}


  
  
tempa= {
  \key c \major \time 2/4
}


introab = \relative c'' {
%  \repeat volta 2 {
  s4. a8 | a c c e ~| e2 | r |
  r4. f8 | e d e c ~|c2 | s | s
  
%  }
  
}

introa = \relative c'' {
%  \repeat volta 2 {
  s4. c8 | c e e g ~| g2 | r |
  r4. a8 | g f g e ~|e2 | r | r |
  
%  }
  
}


parteum = \relative c'' {
  \tempo 4 = 120
  g'8 g a g | f16 e8 d16 c4 \break
  a'8 g f e | f16 e8 d16 c4
}

parteumb = \relative c'' {
  s2 s2 s2 s2
}

partedois = \relative c'' {
  \break
  c8 c e g | g2 | a8 g f g | e2
}

partedoisb = \relative c'' {
  a8 a c e | e2 | f8 e c d | c2
}


partetres = \relative c'' {
  \break 
  c8 c e g | fis16 e8 d16 c4 | 
  a'8 g fis e | fis16 e8 d16 c4 \break
}

\book {
  \bookOutputName "coco_de_toreh_C"
  \header {
     instrument = "Para instrumentos em C"
  }
  \score {
    <<
      \new TimeSig \compassoseparado
      \new Staff <<
                  \tempa
        \new Voice = "terca" {
          \voiceOne
          \introa
          \repeat volta 2 {
          \parteumb
          \partedois
          }
          \partetres
          \partedois
          \bar "|."
        }
        \new Voice = "trombone" {
          \voiceTwo
          \introab
          \repeat volta 2 {
          \parteum
          \partedoisb
          }
          \parteumb
          \partedoisb
%          \transpose f es {
%            \parteum
%            \partedois
%            \refrao
%          }
        }
        
%        \addlyrics \letratoda
      >>
      
    >>
    \layout{
      	
    }
    \midi {}
  }
}

\book {
  \bookOutputName "coco_de_toreh_Eb"
  \header {
     instrument = "Para instrumentos em Eb"
  }
  \score {
    <<
      \new TimeSig \compassoseparado
      \new Staff <<
                   \transpose bes g {\tempa}
        \new Voice = "terca" {
          \voiceOne
          \transpose bes g {
          \introa
          \repeat volta 2 {
          \parteumb
          \partedois
          }
          \partetres
          \partedois
          \bar "|."
          }
        }
        \new Voice = "trombone" {

          \voiceTwo
                    \transpose bes g {
          \introab
          \repeat volta 2 {
          \parteum
          \partedoisb
          }
          \parteumb
          \partedoisb
          }
%          \transpose f es {
%            \parteum
%            \partedois
%            \refrao
%          }
        }
%        \addlyrics \letratoda
      >>
      
    >>
      \layout{
      	
    }
  }
}

\book {
  \bookOutputName "coco_de_toreh_Bb"
  \header {
     instrument = "Para instrumentos em Bb"
  }
  \score {
    <<
      \new TimeSig \compassoseparado
      \new Staff <<
                   \transpose bes c {\tempa}
        \new Voice = "terca" {
          \voiceOne
          \transpose bes c {
          \introa
          \repeat volta 2 {
          \parteumb
          \partedois
          }
          \partetres
          \partedois
          \bar "|."
          }
        }
        \new Voice = "trombone" {

          \voiceTwo
                    \transpose bes c {
          \introab
          \repeat volta 2 {
          \parteum
          \partedoisb
          }
          \parteumb
          \partedoisb
          }
%          \transpose f es {
%            \parteum
%            \partedois
%            \refrao
%          }
        }
%        \addlyrics \letratoda
      >>
      
    >>
      \layout{
      	
    }
  }
}


\version "2.23.3"
