---
title: Repertório
layout: page
image: /assets/uploads/escola-avancada.jpeg
---


Partituras e gravações para os ensaios:

[Partituras](https://drive.google.com/drive/folders/1WQnXZ6-MPfVr_SznweWNc26eccx2P0jZ)

---
# Playlist
<iframe src="https://open.spotify.com/embed/playlist/0rFHjOhb14IEhB2wKGrxLc?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

---
[Fotos do ensaio com abraço na praça da Banca Colina](https://drive.google.com/drive/folders/1B67eUuTKYaZIpmFebtjDi3jf8Bj-jwf_)

# Vídeo Fogo
<iframe src="https://drive.google.com/file/d/14ORP3R3BA5tv55v1eewhx-F7gSSdmBTB/preview" width="640" height="480" allow="autoplay"></iframe>
