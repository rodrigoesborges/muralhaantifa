---
title: A muralha
layout: page
image: /assets/uploads/cropped-esoview1.jpg
---
## A muralha antifascista
---
A Muralha Antifa é a união orgânica de membros de fanfarras e afins para, em pé de igualdade, tocarem em protestos. No primeiro ensaio na Colina, UnB, tínhamos trompetes, 1 trombone, 1 bombardino e uma caixa. Já no segundo tínhamos trompetes, 3 trombones, 2 tubas, 2 caixas, 2 sax alto, 1 bombardino, 1 clarinete. E assim vamos crescendo a muralha… 

Junte-se você também à Muralha: é só colar com um instrumento ou faixa, cartaz, fantasia ou performance de protesto anti-fascista.

Somos um grupo aberto, divulgado boca a boca e nas manifestações. Nosso ponto de encontro é a rua: em ensaios ou protestos.

Se quiserem nos chamar de fanfarra, que sejamos então uma fanfarra pirata: nosso repertório é o que estiver saindo no ensaio.

_"Aterrorizando geral,_
_O problema é que a gente toca mal._
_A solução é que tocamos popular,_
_Tocamos juntos, em protestos._
_E protestamos com Carnaval!"_


**Nossos Objetivos**
- Disseminar a arte como parte integral e potencializadora das manifestações populares
- Pela democracia e pela democratização
- Fora Bolsonaro e Mourão!

