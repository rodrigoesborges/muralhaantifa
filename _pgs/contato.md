---
title: Contato com A Muralha
layout: page
image: /assets/img/muralha_logo.png
---
## Converse conosco
---

Entre em contato por e-mail ou Whatsapp:
<p align="center">
<a href="mailto:contato@muralhaantifa.art.br"><i class="fas fa-at"></i>Mande um e-mail</a><br>
<a href="https://api.whatsapp.com/send?phone=5561981398977&text=Sobre+a+muralha"><i class="fab fa-whatsapp"></i> Converse no Whatsapp</a><br>
<a href="https://www.instagram.com/muralhaantifa/"><i class="fab fa-instagram"></i> ...Ou no Instagram!</a><br>
</p>

