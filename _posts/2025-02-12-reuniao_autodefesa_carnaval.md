---
title: "Ensaio 17 de fevereiro 2025 na Galeria dos Estados"
date: 2025-02-12T08:34:23.674Z
image: /assets/img/defesapedra.jpg
author: fred
tags:
  - Ensaios
---
*Saudações, Muralha!*

Esta quarta-feira, 12/02, 19h, 
chamamos uma reunião de músicos e trabalhadores
ambulantes, caixeiros, na Galeria dos Estados, 
 contra repressão do GDF ao carnaval e 
pré-carnaval de rua!

