---
title: "Ato pela terra 09/03"
date: 2022-03-07T08:34:23.674Z
image: /assets/img/atocaetano.jpg
author: rodrigoesborges
tags:
  - Atos
---
Nesta quarta-feira, às 15h, haverá o Ato pela terra contra o pacote de destruição,  em frente ao Congresso Nacional.
O ato contará com vários artistas e movimentos engajados como Caetano Veloso,  
APIB - SEU JORGE - MST - UNE - GREENPEACE - LAZARO RAMOS - 342 AMAZONIA - BELA GIL
MIDIA NINJA - NANDO REIS- MTST - CLIMAINFO - LETICIA SABATELLA - OBSERVATORIO DO CLIMA - CIMI - BRUNO GAGLIASSO - NATIRUTS - MARIA GADÚ -
CHRISTIANE TORLONI
*Protesto de Quarta 09/03 - Ato pela Terra*
_Nosso ponto de encontro: 15h30 no Museu da República_

No ensaio desse Sábado ensaiamos 4 músicas para tocar caso haja espaço e oportunidade. São elas:
- Du Mouvement Social
- Lucro
- Vaca profana
- Reconvexo

*Atenção para Du Mouvement. São três partes em loop:
A - Base
B - Voz "fora Bozo"
C - Riff
Cada parte são 4 tempos
Vamos juntos! 🤝🏽
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    