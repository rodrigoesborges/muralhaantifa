---
title: "Ensaio 11 de junho na Galeria dos Estados"
date: 2022-06-12T08:34:23.674Z
image: /assets/img/bancacolina.jpg
author: ananda
tags:
  - Ensaios
---
*Saudações, Muralha!*

Segue resumo do que foi resolvido na reunião pós-ensaio:

##Muralhanews - Ensaio 11/06/22 - 🧱✊

1 -Ensaios: com hora pra começar e terminar, para as pessoas poderem se programar e a Muralha não se desmobilizar.
1.1-Sábados na Galeria dos Estados
Começar às 17h30
Terminar 20h
1.2-Voltar a serem Quinzenais, podendo voltar a ser semanais quando nos aproximarmos de alguma demanda: Festival Marco Zero (vide abaixo), Honk Bsb, Eleições, etc.
1.3-Próximos ensaios:
25/06
09/07
23/07
…

2 -Balanço Honk POA: deu certo a proposta da Muralha no Honk! Foi tudo de bom!
2.1-Nossa proposta não foi pretensiosa pois esse espaço de união de fanfarras para ensaiarem, trocarem ideia e se juntarem para tocar em protestos de fato é uma novidade e não estava rolando em outras cidades do Brasil, em Porto Alegre por exemplo.
2.2-O grande número do pessoal da performance e de músicos que colaram juntos no cortejo de Domingo foi um ponto alto, e não aconteceu apenas espontanemente: foi fruto da nossa proposta para a organização do Honk POA, das nossa conversas com as pessoas lá, da distribuição dos tijolinhos, etc.
2.3-Outro ponto alto, no mesmo Domingo, foi a Construção/Deus Lhe Pague debaixo do viaduto.
2.4-Sobre nossa identidade visual, apesar da dificuldade de transporte, a faixa cria uma estética forte de protesto.
Foi sugerido: alguns músicos utilizarem coletes com placa afixadas nas costas, mas com um pequeno estandarte no lugar da placa.
2.5-Os lambes que fizemos dias antes ao longo do trajeto de Domingo foram um aprendizado que podemos pensar em repetir.
2.6-O formato de cortejo de protesto do Honk POA também foi um aprendizado: percurso com significado histórico, atos performáticos entre os cortejos das fanfarras.
2.7-Propostas para próximos Honks: divulgar mais vezes nossa setlist e fazer acesso às partituras ser mais imediato no site, link no Insta.
Levar panfletos com nosso setlist na ordem em que vamos tocar, com textos e/ou zine com nossas pautas de protesto.
2.8-Questões e relatos sobre segurança, tanto dos ensaios quanto das tocadas, ficaram de ser trazidas em próximas conversas.

3-Balanço Marcha da Maconha:
Poucos integrantes puderam ir na marcha, e foi discutida a importância de se tocar o repertório em protestos mesmo em formações reduzidas.

4-Agenda de mobilização, até o momento:
A Muralha nasceu e funciona por demanda, então é importante termos o nosso calendário de mobilização, com as demandas que o grupo quer dar conta, para além de eventuais convites e convocações de protesto que a gente possa querer aderir.
 
4.1-Julho: Parada Gay no Domingo, 3 de Julho.
Rodox está indo atrás de mais informações. A princípio, só temos um ensaio marcado para antes: Sábado 25/06.

4.2-Agosto: Quinta edição do Festival de dança Marco Zero, de 23 a 30 de Agosto.
A proposta do festival é projetar a presença das artes para além dos limites postos por teatros, galerias e salas de espetáculos, transformar o olhar das pessoas em relação à paisagem urbana: https://www.cadebrasilia.com/2022/05/festival-marco-zero-de-danca-em.html
Marcelle nos falou do contexto (mais informações no link) e da história desse festival, e nos fez um convite pra participar, que aceitamos. 
É um desafio para a Muralha, pois participando seremos responsáveis tanto pela música quanto pela parte performática!

Ponderamos que, assim como a participação no Honk POA foi importante para a Muralha investir em seu lado musical, a participação no Marco Zero será uma excelente oportunidade pra Muralha investir em seu lado performático, um elemento essencial pra tocadas de protesto, em parceria com grupos de dança/circo da cidade que se interessem em colar. A rodoviária foi sugerida como opção de local. Foi criado um grupo de trabalho para quem quiser se envolver na concepção da Muralha no Festival Marco Zero: https://chat.whatsapp.com/L5aSG31PrYEH31PF6xEEMu
A ideia é levar essa experiência performática pro Honk Bsb também. 

4.3-Setembro: Honk Bsb, de 1 a 4 de Setembro.
Foi sugerido o Cio das Artes em Ceilândia, onde tocamos no 1o de Maio, como um possível lugar para tocadas de Sábado no Honk Bsb.
Se esse local for mesmo escolhido, é importante visitarmos lá pelo menos uma vez antes do Honk Bsb, mesmo que seja em pequenos destacamentos, e nem precisa ser para tocar: pode ser para um baile de hip hop, sarau de poesia, oficina, etc.

4.4-Outubro: eleições, 1o e 2o turnos.

4.5-Janeiro: Lulapalloza.

… 

PS- Se ficou faltando algum assunto que conversamos lá nesse ensaio, comente no grupo da Muralha que a gente envia na Lista de Transmissão.
