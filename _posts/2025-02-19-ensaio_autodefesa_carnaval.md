---
title: "Ensaio 17 de fevereiro 2025 na Banca Colina"
date: 2025-02-19T08:34:23.674Z
image: /assets/img/20250217ensaio.jpg
author: fred
tags:
  - Ensaios
---
*Saudações, Muralha!*

Próxima quarta-feira, 19/02, meio-dia, manifestação contra repressão do GDF!

Para preparar participação da Mureta Muralha Orquestra, 
Ensaiamos 17/02, segunda-feira à noite, 20h, 
na pracinha em frente à banca Colina!
