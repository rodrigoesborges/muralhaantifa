---
title: Conversa Pós-Ensaio - Muralha Antifascista
date: 2021-10-18T18:29:21.302Z
image: /assets/img/bancacolina_2.jpg
author: ananda
tags: []
---

Conversamos sobre o repertório conter músicas de manifestação. Está aberto para ajustes e acréscimos. Novas músicas são bem-vindas. Separamos pequenos blocos musicais intercalados com paradas para intervenções artísticas e no megafone. Henrique ficou de trazer uma ideia no próximo ensaio, dia 06/11. Outras ideias também poderão ser acrescentadas.

*1º bloco*
Pout-porri Da Lama ao Caos/ Caminho do Bem/ Another Brick in The Wall
Du Mouvement Social
Deus Lhe Pague

*2º bloco*
Bella Ciao (a definir o arranjo pela Bloka no sábado)
Povo Unido
Sociedade Alternativa

*3º bloco*
Ciranda, de Lia do Itamaracá (Lorena ficou de colocar as partituras no drive da Muralha)
Divino Maravilhoso
Tropicália

*4º bloco*
Paula e Bebeto
O Vira
Comportamento Geral (vamos ver se sai no próximo ensaio)
Vila Matilde? Du Mouvement Social de novo?
(este bloco não ficou bem fechado)

*5º bloco*
Haitian Fight Song
Comanshe
Joshua

_*Olé Olá*_ ficou de ser puxado aleatoriamente nos blocos.
