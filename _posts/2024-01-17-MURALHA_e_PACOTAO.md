---
title: "Muralha e Pacotão"
date: 2024-01-17T08:34:23.674Z
image: /assets/img/pacotao.jpg
author: ananda
tags:
  - Ensaios
---
*Saudações, Muralha!*

##Muralhanews - Ensaio 11/06/22 - 🧱✊

*🧱 MURALHA e PACOTÃO 🧱*
 Como levar os princípios 
da Muralha para o Carnaval? Como reverenciar quem há 
muito tempo reivindica o Carnaval como ato político? 
Numa tentativa de responder essas perguntas a 
Muralha se propôs a se aproximar do Bloco Pacotão, 
conversando com seus integrantes e reivindicando um 
espaço livre de machismo, racismo, LGBTQFOBIA. E 
para levar em forma de música nossos princípios e 
ideais, compusemos a Marchinha da Muralha 
Antifascista: "Vumbora, bora!" que concorrerá no 
Concurso de Marchinhas do Bloco e tem chance de ser 
escolhida como a marchinha oficial do Carnaval 2024 
do Bloco Pacotão.
*O concurso acontecerá no Conic, sábado 20/01/24, a partir de 11h. Venha participar conosco tocando ou torcendo pela gente!*
