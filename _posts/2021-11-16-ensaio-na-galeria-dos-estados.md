---
title: "Ensaio - Galeria dos Estados"
date: 2021-11-08T17:06:45.674Z
image: /assets/uploads/fotogaleriaestados.jpg
author: ananda
tags:
  - Ensaios
---
*Próximo Ensaio será na Galeria dos Estados*

- *Dia*: segunda-feira 15/11/2021

- *Horário*: das 16h às 19h


- *Como será o rolê*:
Decidimos mudar o local do ensaio: será na Galeria dos Estados. No dia 15, faremos um ensaio valendo lá! Vamos levar faixar e fazer performances. No dia 20, pretendemos fazer um ato na praça Marielle Franco, se não houver ato na Esplanada. A confirmar até o encontro do dia 15


- *Local*
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-47.88556337356568%2C-15.799913739994668%2C-47.88442611694336%2C-15.79812776947225&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/-15.79902/-47.88499">Ver mapa ampliado</a></small>
[Abrir no openstreetmap](https://www.openstreetmap.org/way/280578983)

