---
title: Conversa Pós-Ensaio - Muralha Antifascista
date: 2021-11-08T18:29:21.302Z
image: /assets/img/bancacolina.jpg
author: ananda
tags: []
---

Decidimos mudar o local do ensaio: será na Galeria dos Estados. No dia 15, faremos um ensaio valendo lá! Vamos levar faixar e fazer performances. No dia 20, pretendemos fazer um ato na praça Marielle Franco, se não houver ato na Esplanada. A confirmar até o encontro do dia 15
