---
title: "Ensaio 4 de dezembro"
date: 2021-11-30T18:34:23.674Z
image: /assets/img/fogaleria.gif
author: ananda
tags:
  - Ensaios
---
*Ensaio da Muralha Antifascista este sábado, 04/12, às 16h, na Galeria dos Estados! Bora?!* 🔥😃

- *Dia*: sábado 04/12/2021

- *Horário*: das 16h às 19h

Na reunião pós-ensaio, podemos conversar sobre:
- último ensaio desse ano e também a data pra gente voltar ano que vem 😁
- formas de organização, comunicação, mobilização e expressão que queremos pra Muralha 👍
- balanço da nossa participação nos protestos na Esplanada ⚖️
- balanço dos contatos com movimentos sociais que fizemos antes e nos protestos 📞
- possibilidades de tocadas para o Carnaval 🎊
- o que mais aparecer na roda! 🔎
Vamos juntos! ✊🏾


- Local*
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-47.88556337356568%2C-15.799913739994668%2C-47.88442611694336%2C-15.79812776947225&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/-15.79902/-47.88499">Ver mapa ampliado</a></small>
[Abrir no openstreetmap](htt