---
title: "Ensaios passados"
date: 2025-02-01T21:06:45.674Z
image: /assets/uploads/fotogaleriaestados.jpg
author: rodrigo
tags:
  - Ensaios
---
Ensaios passados:


- Ensaio carnaval - Ter 30/01/24 às 18h - Galeria dos Estados<br>
- <strong>Último ensaio pré-carnaval - Ter 06/02/24 às 18h- Galeria dos Estados</strong><br>
- CARNAVAL PACOTÃO - Ter 13/02/24 !!!<br>
-  Ensaio - Ter 27/02/24 às 18h - Galeria dos Estados<br>
- Ensaio Extra Seg 15/11/23 às 16h - Galeria dos Estados*
- Manifestação Sáb 20/11/23 às 15h - Museu Nacional, Dia
- Ensaio Sáb 04/12/23 às 16h - Galeria dos Estados
- Ensaio Dom 19/12/23 às 10h - Eixão**
- 1o Ensaio 2022 22/01 às 16h
- Sábado 05/02 - 15h Galeria dos Estados
- Sábado 19/02 - 15h Lago Norte
(Descarnaval)
- Sábado 05/03 - 15h Galeria dos Estados
- Sábado 12/03 - 15h Galeria dos Estados
- Sábado 19/03 - 15h Galeria dos Estados
- Sábado 26/03 - 18h Galeria dos Estados
- Sábado 02/04 - 18h Galeria dos Estados
- Ato pela terra - Qua 09/03 às 15h - Congresso Nacional
- Ensaio / Comemoração áries - Sab 09/04 às 18h - Galeria dos Estados
- Ensaios 1o de maio - Sab 23/04 às 18h - Galeria dos Estados
- Ensaios 1o de maio - Sab 30/04 às 18h - Galeria dos Estados
- Tocata 1o de maio - 01/05 às 14h - Cio das Artes - Ceilândia
- Ensaios Honk POA - Sab 07/05 às 18h - Galeria dos Estados
- Ensaios Honk POA - Sab 14/05 às 18h - Galeria dos Estados
- Honk POA - de 18/05 21/05 - Porto Alegre
- Ensaio - Sab 10/06 às 18h - Galeria dos Estados
- Último Ensaio pré-parada - Sáb 25/06 às 18h - Galeria dos Estados
- Parada Gay - Dom 03/07 - Horário e local por confirmar
- Ensaio - Sáb 09/07 às 18h - Galeria dos Estados
- Ensaio prep. Marco Zero - Sáb 23/07 às 18h - Galeria dos Estados
- V Festival Marco Zero - 23 a 30 de Agosto


*Próximo Ensaio será na Galeria dos Estados*

- *Dia*: sábado 30/01/2024

- *Horário*: das 18h às 21h

- *Local*
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-47.88556337356568%2C-15.799913739994668%2C-47.88442611694336%2C-15.79812776947225&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/-15.79902/-47.88499">Ver mapa ampliado</a></small>
[Abrir no openstreetmap](https://www.openstreetmap.org/way/280578983)

