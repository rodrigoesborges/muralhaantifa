---
title: "Ensaio 19 de dezembro no Eixão"
date: 2021-12-05T08:34:23.674Z
image: /assets/img/eixao.jpg
author: ananda
tags:
  - Ensaios
---
*Saudações, Muralha!*

Segue resumo do que foi resolvido na reunião pós-ensaio:

- O último ensaio/ ato desse ano será dia 19/12 (domingo) às 10h no Eixão, na altura da 11 Norte. Todos com a camisa da Muralha, o máximo possível! ✊🏾
- A data definida para a volta dos ensaios ano que vem foi dia 22/01. Vamos tentar entrar em contato com a Administração de Brasília para ativar os banheiros do metrô nos fins de semana. Giu vai tentar através de um contato dela! 😁
- Sobre as formas de organização, comunicação, mobilização e expressão que queremos pra Muralha, decidimos que teremos grupo de Whatsapp fechado que será aberto quando comunicações mais urgentes surgirem. 

Um grupo de Mobilização também está em formação, com Érika da Caixa puxando esta frente. Quem quiser ajudar, pode entrar em contato com ela. Fiquei como responsável pelo grupo da Comunicação, se acheguem! Outros grupos podem ser criados de forma orgânica 👍

- Balanço da nossa participação nos protestos na Esplanada: estreitar relações com a organização dos atos e movimentos sociais, trabalhar mais nosso contexto político, faixas ainda estão em falta. ⚖️
- Possibilidades de tocadas para o Carnaval: ainda incerto. Conversaremos mais para frente por conta da pandemia 🎊
Vamos juntos! 🤝🏽
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    