---
title: "No Pódio do Pacotão!"
date: 2024-01-20T11:40:13.674Z
image: /assets/img/pacotao_vitoria.png
author: ananda
tags:
  - Ensaios
---
*Saudações, Muralha!*


*🧱 MURALHA vai para o pódio PACOTÃO 🧱*
*E aí, Muralha!* 🧱
 
No último sábado, participamos do concurso de marchinhas do Pacotão e subimos no pódio em *3° lugar* 🏆🥉!!!!
Esse resultado foi consequência da nossa dedicação na construção da letra, arranjo e participação nos ensaios para que fosse possível uma apresentação no nível que a Muralha pode oferecer. 

Sendo assim, convidamos a todes para estarem nessa *terça-feira (23), às 19h, na Galeria dos Estados*, no nosso ensaio, onde iremos avaliar se possuímos quórum suficiente para sairmos com o Pacotão no Carnaval. Sua presença é muito importante para que possamos saber se sairemos! Aguardamos você 🫵🏼
