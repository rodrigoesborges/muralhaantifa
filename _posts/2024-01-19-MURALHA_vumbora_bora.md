---
title: "Vumbora,bora! 3° Lugar no Concurso de Marchinhas do Pacotão"
date: 2024-01-20T11:40:13.674Z
image: /assets/img/tocada_pacotao.png
author: ananda
tags:
  - Ensaios
---

📦🏆 “Vumbora, bora!” 3o lugar no concurso da marchinhas do [pacotaofolia](https://instagram.com/pacotaofolia), foi uma honra participar e tocar com vcs 🌈✊🧱

Vumbora, bora,
Sem arregá
Quié pra canalha não governar
Vumbora, bora
É pela vida
Aqui é antifascista!

Assim meu bloco vai morar na rua
Nesse calor internacional
Esse pode ser o fim do mundo
Vamo com tudo pro carnaval!
Que pegue fogo cada vagabundo
Quiainda faz pix praquele verme imundo

Meu corpo quente não quer ventilador
Só quer justiça, paz e amor
Vumbora, bora,
Sem arregá
Quié pra canalha não governar Vumbora, bora
É pela vida
Aqui é antifascista!
Aqui é antifascista!
Aqui é antifascista!

-Partituras e letra completa: na seção de partituras!
