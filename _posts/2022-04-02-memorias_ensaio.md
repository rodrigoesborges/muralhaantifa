---
title: "Ensaio 02 de abril na Galeria dos Estados"
date: 2022-04-04T08:34:23.674Z
image: /assets/img/honkpoa2022.png
author: ananda
tags:
  - Ensaios
---
*Saudações, Muralha!*

Segue resumo do que foi resolvido na reunião pós-ensaio:

*Resumo da Reunião de Sábado 02/04 - Muralha* 
- Um grupo foi criado para tratar da inscrição da Muralha no Honk POA. Giuliana, Helô, Rodox e Lucas. Quem quiser participar, fala com Helô; 
- Foi levantado que podemos ser mais solidários e atentos nas caronas, oferecer e pedir no grupo; 
- Será criada arte dos adereços de tijolinho; 
- Próximo ensaio (09/04 às 18h), teremos festejo dos arian@s no ensaio a partir de 19:30h. Preparem-se! 🔥

